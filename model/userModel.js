const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
	{
		firstName: {
			type: String,
			required: [true, "First Name is required"]
		},
		lastName: {
			type: String,
			required: [true, "Last Name is required"]
		},
		email: {
			type: String,
			required: [true, "Email is required"]
		},
		password:{
			type: String,
			required: [true, "Password is required"]
		},
		isAdmin:{
			type: Boolean,
			default: false
		},
		placeOrder: [
			{
				productId:{
					type: String,
					required:[true, `Product is required`]
				},
				quantity:{
					type: Number,
					required: [true, `Order quantity is required`]
				},
				purchaseOn:{
					type: Date,
					default:new Date()
				}
			}
		]
	})

module.exports = mongoose.model("user", userSchema)