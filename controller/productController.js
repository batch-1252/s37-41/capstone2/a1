const product = require("./../model/product");

module.exports.addProduct = (reqBody) =>{

	let newProduct = new product({
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	})

	return newProduct.save().then((product, error)=>{
		if(error){
			return false
		} else {
			return true
		}
	})

}

module.exports.activeProduct = () => {
    return product.find({isActive: true}).then( result => {
        
        return result
    })
}

module.exports.getSingleProduct = (params) => {

    return product.findById(params.productId).then( result => {
        return result
    })

}

module.exports.editProduct = (params, reqBody) => {

    let updatedProduct = {
        name: reqBody.name,
        description: reqBody.description,
        price: reqBody.price,
        isActive: true,
        createdOn: new Date()
    }

    return product.findByIdAndUpdate(params, updatedProduct, {new: true})
    .then((result, error) => {
        if(error){
            return error
        } else {
            return result
        }
    })
}

module.exports.archiveProduct = (params) => {

    let updatedActiveProduct = {
        isActive: false
    }

    return product.findByIdAndUpdate(params, updatedActiveProduct, {new: true})
        .then( (result, error) => {
            if(error){
                return false
            } else {
                return true
            }
    })
}