const user = require("./../model/userModel");
const product = require("./../model/product");
const bcrypt = require("bcrypt");
const auth = require("./../auth");

module.exports.register = (reqBody) =>{

	let newUser = new user({
		firstName: reqBody.firstName,
		lastName: reqBody.lastName,
		email: reqBody.email,
		password: bcrypt.hashSync(reqBody.password, 10)
	})

	return newUser.save().then((result, error)=>{
		if(error){
			return error
		} else {
			return true
		}
	})

}

module.exports.setUserAsAdmin = (userId)=>{
    return user.findByIdAndUpdate(userId, {isAdmin: true }, {new: true})
	    .then((result, error) => {
	        if(error){
	            return error
	        } else {
	            return result
	        }
	    })
}

module.exports.check = (reqBody)=>{
	return user.find({email: reqBody.email})
	.then((result) =>{
		if(result.lengt !== 0){
			return true
		} else {
			return false
		}
	})
}

module.exports.login = (reqBody) => {
	return user.findOne({email: reqBody.email}).then((result) =>{
		if(result == null){
			return false

		}	else {
			const isPasswordCorrect = bcrypt.compareSync(reqBody.password, result.password)
			
			if(isPasswordCorrect === true){
				return { access: auth.createAccessToken(result.toObject())}
			} else {
				return false
			}
		}
	})
}

module.exports.getProfile = (data) => {
	
	return user.findById(data).then( result => {

		result.password = "******"
		return result
	})
}

module.exports.viewCart = (userId) => {
	return user.findById(userId).then(async user => {
		let cart = await user.placeOrder.map(async item => {
			let result = await product.findById(item.productId)
			return {
				name: result.name,
				price: result.price,
				description: result.description,
				
				quantity: item.quantity,
				totalPrice: result.price * item.quantity,
				purchaseOn: item.purchaseOn
			}
		})
		return await Promise.all(cart)
	})
}

module.exports.addToCart = async (data) => {
	const userSaveStatus = await user.findById(data.userId).then(async user => {
		let placeOrder = user.placeOrder;
		let orderIndex = placeOrder.findIndex(item => item.productId === data.productId); 
		if(orderIndex !== -1) {
			user.placeOrder[orderIndex].quantity++;
		} else {
			user.placeOrder.push({
				productId: data.productId,
				quantity: 1,
			});
		}
		return user.save().then( (user, error) => {
			if(error){
				return false
			} else {
				return true
			}
		})
	})
}