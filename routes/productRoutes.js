// activate using require 
const express = require('express');
const router = express.Router();

// connection to auth.js
const auth = require("./../auth");

// connection to productRoutes.js
const productController = require("./../controller/productController");

router.post("/addProduct", auth.verify, (req, res) => {
	if(!auth.decode(req.headers.authorization).isAdmin) {
		res.send('Unauthorized request');
	}
	productController.addProduct(req.body).then( result=>res.send(result));
})

router.get("/activeProduct", (req, res) => {
	productController.activeProduct(req.body).then((result)=> res.send(result));
})

router.get('/:productId', auth.verify, (req, res) => {
	console.log(req.params);
	productController.getSingleProduct(req.params).then( result => res.send(result))

})

router.put('/edit/:productId', auth.verify, (req, res) => {
	if(!auth.decode(req.headers.authorization).isAdmin) {
		res.send('Unauthorized request');
	}

	productController.editProduct(req.params.productId, req.body).then( result => res.send(result))
})

router.put('/archive/:productId', auth.verify, (req, res) => {
	if(!auth.decode(req.headers.authorization).isAdmin) {
		res.send('Unauthorized request');
	}
	productController.archiveProduct(req.params.productId).then( result => res.send(result))
})



module.exports = router;