const express = require('express');
const router = express.Router();

// connection to auth.js
const auth = require("./../auth");

// connection to controller.js
const userController = require("./../controller/userController");

// registration of new user
router.post("/register", (req, res) =>{
	userController.register(req.body).then(result => res.send(result));
})

// view check user account
router.post("/check", (req, res) =>{
	userController.check(req.body).then(result => res.send(result));
})

router.post("/login", (req, res) =>{
	userController.login(req.body).then(result => res.send(result));
})


router.put("/set-admin/:userId", (req, res) => {
	userController.setUserAsAdmin(req.params.userId).then(result => res.send(result));
})

router.get("/userProfile", auth.verify, (req, res) => {
	const userData = auth.decode(req.headers.authorization)
	userController.getProfile(userData.id).then(result => res.send(result))
})

router.post('/addToCart', auth.verify, (req, res) => {
	if(auth.decode(req.headers.authorization).isAdmin) {
		res.send('Unauthorized request');
	}
	let data = {
		userId: auth.decode(req.headers.authorization).id,
		productId: req.body.productId
	}
	userController.addToCart(data).then( result => res.send(result))
})

router.get('/view-cart', auth.verify, (req, res) => {
	
	
	let userId = auth.decode(req.headers.authorization).id;
	userController.viewCart(userId).then( result => res.send(result))
})

module.exports = router;