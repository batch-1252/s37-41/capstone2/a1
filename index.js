// list of required variable
const express = require("express");
const mongoose = require("mongoose");
const PORT = process.env.PORT || 4000;
const app = express();
const cors = require("cors");

// variable connection	
const userRoutes = require("./routes/userRoutes");
const productRoutes = require("./routes/productRoutes");


// Middlewares
app.use(express.json());
app.use(express.urlencoded({extended:true}));
app.use(cors());


// Mongoose Connection
mongoose.connect("mongodb+srv://daliuz:517892021@cluster0.mlkat.mongodb.net/e-commerce?retryWrites=true&w=majority",
		{
			useNewUrlParser: true,
			useUnifiedTopology: true
		})
.then(()=>console.log(`Connected to Database`))
.catch((error)=> console.log(error))


// Routes
app.use("/api/users", userRoutes);
app.use("/api/products", productRoutes);


app.listen(PORT, ()=>console.log(`Connected to Server at PORT ${PORT}`));
